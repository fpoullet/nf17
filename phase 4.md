# MISE AU POINT DES RENDUS POUR LA PHASE FINALE

Phase 1: NDC, MCD

Phase 2: NDC, MCD, MLD, SQL (CREATE/INSERT)

Phase 3: NDC, MCD, MLD, SQL (CREATE/INSERT), NoSQL, Requêtes(SQL/NoSQL)


## Phase 1: (notée 2/3)

### NDC: 
Reprise avec les nouveaux choix de modélisation.

### MCD:
Corrigé avec les nouveaux choix de modélisation, l'ajout des contraintes manquantes, format PNG ajouté.

## Phase 2: (notée 1.5/3 + 0.5/3 = 1/3)

### MLD:
Ajout des contraintes manquantes et correction des mauvaises contraites.

### SQL:
Le SQL a été rendu en retard, il a été revu et les contraintes simples ont été rajoutées. Les contraintes complexes qui nécessitent des triggers n'ont pas été implémentés. La désaccentuation a été faite.

## Phase 3: (notée 2/3 + 0/3 = 1/3)

### NoSQL:

le JSON a été rendu en retard, il comportait une transformation datatype en attribut JSON des classes Chaine et GrilleSemaine, auusi, la classe Audience était absorbée en attribut JSON de GrilleSemaine. Après le retour lundi, j'ai réalisé une nouvelle implémentation NoSQL avec en plus GrilleSemaine comme attribut JSON de Chaine. (L'ancienne implémentation est toujours disponible dans le dossier OldJSON)

### Requêtes:

Une requête avait mal été interprétée. La bonne a été ajoutée sur le SQL et le NoSQL
(l'ancienne requête s'appelle requête 0 et est toujours disponible).
