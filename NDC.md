# Note de clarification, Gestion de chaînes de télévision

## Contexte du projet
                        
Dans le cadre de l'UV NF17, nous sommes amenés à mettre en place une base de données pour une société d'édition de services vous demandent sur les chaînes et les programmes qu'elle édite       

### Acteurs du projet

Maîtrise d'ouvrage :
- M. Lussier, représentant de la société

Maîtrise d'oeuvre :
    
- F. Poullet-Pagès, développeur


### Objet du projet


Ce projet a pour but d'aider une société à gérer la programation de ses chaines de television, l'organisation de ses emissions, et mettre en place des outils d'analyse d'audience.

La phase de formalisation du projet a été menée avec le maître d'ouvrage.       

### Objets principaux de la base de données
Les principaux objets manipulés par la base de données sont les suivants :

- Chaines
- Emissions
- Intervenants

### Besoins du client
- Gérer les chaînes de la société d'édition
- Gérer les grilles de programme de chaque chaîne
- Mettre en place des outils d'analyse statistique, comme la répartition des horaires par public visé dans la grille de la chaîne (par exemple : 23 heures pour les enfants, 45 heures pour les adultes, 30 heures pour les publics féminins, etc.), les émissions qui ont eu le plus de spectateurs dans les trois dernières années, la chaîne la plus populaire (en proportion de leurs horaires de diffusion), etc.

### Gestion de la programmation des chaines
- La société possède plusieurs chaines
- Chaque chaine possède un nom qui l'identifie, un directeur un siège social une plage de diffusion et au moins un moyen de diffusion
- Chaque année, une nouvelle grille semaine est éditée permetant d'associer une émission hebdomadaire et une chaine sur un jour de la semaine à une heure définie.
- La grille semaine associe une emission à une chaine sur un jour précis et un horaire précis
- Les films et les journaux sont particuliers car ils varient d'une semaine à l'autre, on les distingue alors avec un attribut semaine de diffusion de 0 à 51), membre de la clé de la grille, pour les séries et les documentaires il est à 0 (ce qui semble logique, car la première semaine de diffusion d'une série ou d'un documentaire est lors de la semaine 0 de l'année).
- Pour les 52 semaines de l'année, on note l'audience de l'émission dans AudienceSemaine

### Organisation des emissions
- Une émission possède un nom qui l'identifie et un pays
- Il existe une version étrangère si le pays n'est pas la France
- Une emission est soit un documentaire, un journal télévisé, une série ou un film.
- En fonction de l'un des 4 types énoncés elle possède différentes caractéristiques: une catégorie, un studio etc..
- En fonction de l'un des 4 types énoncés elle possède différents intervenants: acteur, animateur etc...
- Chaque intervenant est identifié par un nom et un prénom, il possède une nationalité

### Analyse d'audience (requêtes SQL)
- Chaque emission possède un compteur de téléspectateurs et un public visé
- Pour la grille semaine d'une chaine sur une année donnée, on veut compter le nombre d'heures visée pour chaque public
- Sur les trois dernières années, les émissions avec le plus de téléspectateurs
- La chaine la plus populaire (soit le total des téléspectateurs sur le total de la plage de diffusion de la chaine)

