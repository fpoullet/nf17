# Projet personnel NF17

## Félix POULLET-PAGÈS

### UV : NF17 TD1

### Projet N°4

### Nom du projet : Gestion de chaînes de télévision

### Chargé de TD : Benjamin Lussier

| Nom | Prénom | Rôle |
| ------ | ------ | ------ |
| Lussier | Benjamin | Client | 
| Poullet-Pagès | Félix | Développeur | 

## Description du dépot :
Le dépot est constitué de plusieurs fichiers et dossiers.
Ce projet sera réalisé en SQL pour le SGBD Postgres.


### 1) NDC

Note de clarification avec développement rédigé des hypothèses de modélisation.

### 2) MCD

Modélisation conceptuelle de données, UML diagramme de classe en format ```pdf```

### 3) MLD

Modélisation logique relationnelle de données, avec normalisation

## Attentes du client :

### Objectif

Une société d'édition de services vous demandent de réaliser une base de données sur les chaînes et les programmes qu'elle édite.
Contexte

La société d'édition de services cliente possède plusieurs chaînes de télévision, et envisage d'en créer de nouvelles et peut être d'en supprimer certaines (mais elle ne veut pas supprimer les données associées !).

Chaque chaîne a un directeur des programmes, un siège social, des moyens de diffusion (terrestre, satellite, satellite HD, etc.) et une plage de diffusion (les horaires pendant lesquels elle émet). Elle a également une grille de programmes, qui indique quelles émissions sont diffusées à quel moment. Une grille est renouvelée chaque année, et contient une liste d'émissions avec leur jour et leurs horaires : séries, documentaires, films, et journaux télévisés.

Chaque émission a un nom et un pays d'origine. Si le pays d'origine est étranger, l'émission peut être en version originale ou en version sous-titrée. Les séries, documentaires et films ont tous un studio de production. Les documentaires et les films ont un réalisateur. Les journaux télévisés et certains documentaires ont un animateur. Les séries et les films peuvent avoir une liste d'acteurs célèbres, et les journaux télévisés une liste d'invités. Les documentaires ont un thème (économie, animalier, politique) et les séries un type (drame, dessin animé, action). Les séries, documentaires et journaux télévisés sont les mêmes toutes les semaines d'une année, mais les films peuvent être différents chaque semaine. Enfin, chaque émission a un ou plusieurs publics visés : enfants, adultes, personnes âgées, public masculin, public féminin, etc. La chaîne enregistre aussi le nombre de spectateurs de chaque émission pour chaque semaine.

La société d'édition veut garder les noms, prénoms et nationalité de tous les acteurs, invités, réalisateurs et animateurs de leurs émissions. Elle veut garder en plus le numéro de téléphone des animateurs et des invités, pour pouvoir les contacter rapidement. Finalement, les acteurs et les animateurs sont populaires auprès d'un ou plusieurs publics, comme les émissions.

### Besoins

- Gérer les chaînes de la société d'édition
- Gérer les grilles de programme de chaque chaîne
- Mettre en place des outils d'analyse statistique, comme la répartition des horaires par public visé dans la grille de la chaîne (par exemple : 23 heures pour les enfants, 45 heures pour les adultes, 30 heures pour les publics féminins, etc.), les émissions qui ont eu le plus de spectateurs dans les trois dernières années, la chaîne la plus populaire (en proportion de leurs horaires de diffusion), etc.
