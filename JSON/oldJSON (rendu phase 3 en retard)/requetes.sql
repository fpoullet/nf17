--Requete 0: Pour la grille semaine d'une chaine sur une annee donnee, on veut compter le nombre d'heures visee pour chaque public


SELECT
	public as "Profil du public vise",
	EXTRACT(HOUR FROM(SUM(CAST(heureDiffusion->>'fin' AS TIME)-CAST(heureDiffusion->>'debut' AS TIME)))) AS "Nombre d'heures visees"
	
FROM GrilleSemaine INNER JOIN PublicVise ON GrilleSemaine.emission = PublicVise.emission
WHERE CAST(audiences->>'date' AS DATE) BETWEEN '2020-01-01' AND '2020-12-31'
GROUP BY public
ORDER BY "Nombre d'heures visees" DESC;

---Requete 1: La repartition des horaires par public vise dans la grille de la chaîne

SELECT
	chaine AS "Chaine",
	public as "Profil du public vise",
	SUM(CAST(heureDiffusion->>'fin' AS TIME)-CAST(heureDiffusion->>'debut' AS TIME)) AS "Nombre d'heures visees"
FROM GrilleSemaine
INNER JOIN PublicVise ON GrilleSemaine.emission = PublicVise.emission
GROUP BY public, chaine
ORDER BY chaine;

---Requete 2: Sur les trois dernieres annees, les emissions avec le plus de telespectateurs

SELECT
	emission AS "Top 10 des emissions à succes (sur les 3 dernieres annees)",
	CAST(audiences->>'audience' AS INTEGER) AS "Nombre de telespectateurs"

FROM GrilleSemaine
WHERE CAST(audiences->>'date' AS DATE) BETWEEN '2018-01-01' AND '2020-12-31'
ORDER BY "Nombre de telespectateurs" DESC
LIMIT 10;

---Requete 3: La chaine la plus populaire (soit le total des telespectateurs sur le total de la plage de diffusion de la chaine)

SELECT
	nom AS "Chaine",
	EXTRACT(HOUR FROM(SUM(CAST(heureDiffusion->>'fin' AS TIME)-CAST(heureDiffusion->>'debut' AS TIME)))) AS "Nombre d'heures",
	SUM(CAST(audiences->>'audience' AS INTEGER)) AS "Total de telespectateurs",
	(SUM(CAST(audiences->>'audience' AS INTEGER)) / EXTRACT(HOUR FROM(SUM(CAST(heureDiffusion->>'fin' AS TIME)-CAST(heureDiffusion->>'debut' AS TIME))))) AS "Score de popularite"
	
FROM Chaine LEFT JOIN GrilleSemaine ON GrilleSemaine.chaine=Chaine.nom
GROUP BY Chaine.nom
ORDER BY "Score de popularite" DESC;



