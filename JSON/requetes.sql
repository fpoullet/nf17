---Requete 1: La repartition des horaires par public vise dans la grille de la chaîne

SELECT
	nom AS "Chaine",
	public as "Profil du public vise",
	SUM(CAST(heure->>'fin' AS TIME)-CAST(heure->>'debut' AS TIME))
FROM Chaine, (JSON_TO_RECORDSET(Chaine.grillesemaine) AS Grille (emission VARCHAR, jour VARCHAR, heure JSON, audience JSON) INNER JOIN PublicVise ON Grille.emission = PublicVise.emission), JSON_TO_RECORDSET(Grille.audience) AS Audience (date DATE, audience INTEGER)
GROUP BY public, nom
ORDER BY nom;


---Requete 2: Sur les trois dernieres annees, les emissions avec le plus de telespectateurs

SELECT
	emission AS "Top 10 des emissions à succes (sur les 3 dernieres annees)",
	Audience.audience AS "Nombre de telespectateurs"
FROM Chaine, JSON_TO_RECORDSET(Chaine.grillesemaine) AS Grille (emission VARCHAR, jour VARCHAR, heure JSON, audience JSON), JSON_TO_RECORDSET(Grille.audience) AS Audience (date DATE, audience INTEGER)
WHERE date BETWEEN '2018-01-01' AND '2020-12-31'
ORDER BY Audience.audience DESC
LIMIT 10;

---Requete 3: La chaine la plus populaire (soit le total des telespectateurs sur le total de la plage de diffusion de la chaine)

SELECT
	nom AS "Chaine",
	EXTRACT(HOUR FROM(SUM(CAST(heure->>'fin' AS TIME)-CAST(heure->>'debut' AS TIME)))) AS "Nombre d'heures",
	SUM(Audience.audience) AS "Total de telespectateurs",
	(SUM(Audience.audience) / EXTRACT(HOUR FROM(SUM(CAST(heure->>'fin' AS TIME)-CAST(heure->>'debut' AS TIME))))) AS "Score de popularite"

FROM Chaine, JSON_TO_RECORDSET(Chaine.grillesemaine) AS Grille (emission VARCHAR, jour VARCHAR, heure JSON, audience JSON), JSON_TO_RECORDSET(Grille.audience) AS Audience (date DATE, audience INTEGER)
GROUP BY nom
ORDER BY "Score de popularite" DESC;
