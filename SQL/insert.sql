INSERT INTO "societe"("nom")
VALUES
('Canal'),
('France Television'),
('M6 Groupe'),
('TF1 Groupe');

INSERT INTO "invite"("nom","prenom","nationalite","telephone")
VALUES
('Deleuze','Gilles','France',40201),
('Foucault','Michel','France',4036),
('Kafka','Franz','France',5030),
('Lordon','Frederic','France',604);

INSERT INTO "animateur"("nom","prenom","nationalite","telephone")
VALUES
('Klein','Melani','France',2001),
('Klein','Naomi','France',70596),
('Newton','Isaac','France',34306),
('Pujadas','David','France',5049);

INSERT INTO "acteur"("nom","prenom","nationalite")
VALUES
('Bon','François','France'),
('Lheureux','Ombeline','France'),
('Migairou','Sarah','France'),
('Savovic','Paul-Alexis','France');

INSERT INTO "realisateur"("nom","prenom","nationalite")
VALUES
('Bardon','Paul','France'),
('Melies','Georges','France'),
('Pages','Luc','France'),
('Pages','Sylvie','France');

INSERT INTO "directeur"("nom","prenom","nationalite")
VALUES
('Courtier','Philippe','France'),
('Etchebest','Philippe','France'),
('Katerine','Philippe','Espagne'),
('Poutou','Philippe','France');

INSERT INTO "chaine"("nom","nomsociete","nomdirecteur","prenomdirecteur","siegesocial","plagediffusiondebut","plagediffusionfin")
VALUES
('Canal+','Canal','Katerine','Philippe','Ile-Tudy','05:00:00','23:59:59'),
('France 2','France Television','Poutou','Philippe','Heugueville','00:00:00','23:59:59'),
('M6','M6 Groupe','Etchebest','Philippe','Paris','11:00:00','23:00:00'),
('TF1','TF1 Groupe','Courtier','Philippe','Rouen','07:00:00','23:00:00');

INSERT INTO "moyendiffusion"("type")
VALUES
('Free'),
('IP TV'),
('Molotov'),
('Numericable'),
('Orange'),
('Satellite'),
('TNT');

INSERT INTO "emission"("nom","pays","vo")
VALUES
('20h','France',NULL),
('Cash Investigation','France',NULL),
('Masterchef','France',NULL),
('Titanic','USA',TRUE),
('Urgences','USA',FALSE),
('Ushuaia','France',NULL),
('A+ Pollux','France',NULL);

INSERT INTO "journal"("nom","numero","nomanimateur","prenomanimateur")
VALUES
('20h',2,E'Pujadas','David'),
('20h',1,E'Pujadas','David');

INSERT INTO "documentaire"("nom","studio","theme","nomanimateur","prenomanimateur")
VALUES
('Ushuaia','Les Lheureux','Politique','Klein','Naomi'),
('Cash Investigation','Les Gobelins','Economie','Newton','Isaac');

INSERT INTO "serie"("nom","studio","type","nomrealisateur","prenomrealisateur")
VALUES
('Masterchef','TF1','Tele Realite','Bardon','Paul'),
('Urgences','Hollywood','Action','Pages','Luc');

INSERT INTO "film"("nom","studio","nomrealisateur","prenomrealisateur")
VALUES
('Titanic','Hollywood','Pages','Luc'),
('A+ Pollux','La Femis','Pages','Luc');

INSERT INTO "public"("type")
VALUES
('Enfants'),
('Adolescents'),
('Etudiants'),
('Jeunes Adultes'),
('30-40'),
('Seniors');

INSERT INTO "publicvise"("emission","public")
VALUES
('20h','Seniors'),
('20h','Etudiants'),
('Masterchef','Adolescents'),
('Urgences','Etudiants'),
('Ushuaia','30-40'),
('Ushuaia','Seniors'),
('A+ Pollux','Etudiants');

INSERT INTO "emissionaimee"("emission","public")
VALUES
('20h','Enfants'),
('20h','Etudiants'),
('Masterchef','Adolescents'),
('Urgences','Etudiants'),
('Ushuaia','30-40'),
('Ushuaia','Seniors'),
('Masterchef','30-40');

INSERT INTO "grillesemaine"("chaine","emission","semainediffusion","jourdiffusion","heurediffusiondebut","heurediffusionfin")
VALUES
('Canal+','Titanic',4,E'Lundi','21:00:00','23:00:00'),
('Canal+','Urgences',0,E'Mardi','20:00:00','22:00:00'),
('TF1','Ushuaia',0,E'Mercredi','14:00:00','16:00:00'),
('TF1','20h',3,E'Jeudi','20:00:00','21:00:00'),
('France 2','20h',5,E'Vendredi','20:00:00','20:30:00'),
('France 2','Cash Investigation',0,E'Samedi','20:32:00','22:00:00'),
('M6','Masterchef',0,E'Dimanche','16:00:00','18:00:00'),
('M6','A+ Pollux',5,E'Jeudi','10:00:00','12:00:00');

INSERT INTO "audiencesemaine"("date","audience","chaine","emission","semaine","jour","heuredebut","heurefin")
VALUES
('2020-03-28',10000,E'Canal+','Titanic',4,E'Lundi','21:00:00','23:00:00'),
('2020-03-29',2000,E'Canal+','Urgences',0,E'Mardi','20:00:00','22:00:00'),
('2020-03-28',1000,E'France 2','20h',5,E'Vendredi','20:00:00','20:30:00'),
('2020-03-29',10000,E'France 2','Cash Investigation',0,E'Samedi','20:32:00','22:00:00'),
('2020-03-28',200,E'M6','A+ Pollux',5,E'Jeudi','10:00:00','12:00:00'),
('2020-03-29',10000,E'M6','Masterchef',0,E'Dimanche','16:00:00','18:00:00'),
('2020-03-28',2000,E'TF1','20h',3,E'Jeudi','20:00:00','21:00:00'),
('2020-03-29',20000,E'TF1','Ushuaia',0,E'Mercredi','14:00:00','16:00:00');

INSERT INTO "participe"("invitenom","inviteprenom","journal","numero")
VALUES
('Deleuze','Gilles','20h',1),
('Foucault','Michel','20h',2),
('Kafka','Franz','20h',1),
('Lordon','Frederic','20h',2);

INSERT INTO "seriejouee"("acteurnom","acteurprenom","serie")
VALUES
('Bon','François','Masterchef'),
('Lheureux','Ombeline','Masterchef'),
('Migairou','Sarah','Urgences'),
('Savovic','Paul-Alexis','Urgences');

INSERT INTO "filmjoue"("acteurnom","acteurprenom","film")
VALUES
('Bon','François','A+ Pollux'),
('Lheureux','Ombeline','A+ Pollux'),
('Migairou','Sarah','Titanic'),
('Savovic','Paul-Alexis','Titanic');

INSERT INTO "acteuraime"("acteurnom","acteurprenom","public")
VALUES
('Bon','François','Jeunes Adultes'),
('Lheureux','Ombeline','Seniors'),
('Migairou','Sarah','Enfants'),
('Savovic','Paul-Alexis','Seniors'),
('Lheureux','Ombeline','Etudiants'),
('Lheureux','Ombeline','Jeunes Adultes');

INSERT INTO "animateuraime"("animateurnom","animateurprenom","public")
VALUES
('Klein','Melani','30-40'),
('Klein','Naomi','Jeunes Adultes'),
('Newton','Isaac','Adolescents'),
('Pujadas','David','Seniors');


