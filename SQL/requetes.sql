--Requete 0: Pour les  grilles semaines sur une annee donnee, on veut compter le nombre d'heures visee pour chaque public

SELECT
	public as "Profil du public vise",
	EXTRACT(HOUR FROM(SUM(heurefin - heuredebut))) AS "Nombre d'heures visees"
FROM AudienceSemaine
INNER JOIN PublicVise ON AudienceSemaine.emission = PublicVise.emission
WHERE date BETWEEN '2020-01-01' AND '2020-12-31'
GROUP BY public
ORDER BY "Nombre d'heures visees" DESC;

---Requete 1: La repartition des horaires par public vise dans la grille de la chaîne

SELECT
	chaine AS "Chaine",
	public as "Profil du public vise",
	SUM(heurefin - heuredebut) AS "Nombre d'heures visees"
FROM AudienceSemaine
INNER JOIN PublicVise ON AudienceSemaine.emission = PublicVise.emission
GROUP BY public, chaine
ORDER BY chaine;

---Requete 2: Sur les trois dernieres annees, les emissions avec le plus de telespectateurs

SELECT
	emission AS "Top 10 des emissions à succes (sur les 3 dernieres annees)",
	audience AS "Nombre de telespectateurs"
FROM audiencesemaine
WHERE date BETWEEN '2018-01-01' AND '2020-12-31'
ORDER BY audience DESC
LIMIT 10;

---Requete 3: La chaine la plus populaire (soit le total des telespectateurs sur le total de la plage de diffusion de la chaine)

SELECT
	nom AS "Chaine",
	EXTRACT(HOUR FROM(plagediffusionfin - plagediffusiondebut)) AS "Nombre d'heures",
	SUM(audience) AS "Total de telespectateurs",
	(SUM(audience) / EXTRACT(HOUR FROM(plagediffusionfin - plagediffusiondebut))) AS "Score de popularite"
FROM Chaine
LEFT JOIN AudienceSemaine ON AudienceSemaine.chaine=Chaine.nom
GROUP BY chaine.nom
ORDER BY "Score de popularite" DESC;