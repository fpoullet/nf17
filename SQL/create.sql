DROP TABLE IF EXISTS Societe cascade;
DROP TABLE IF EXISTS Chaine cascade;
DROP TABLE IF EXISTS MoyenDiffusion cascade;
DROP TABLE IF EXISTS DiffuseVers cascade;
DROP TABLE IF EXISTS Emission cascade;
DROP TABLE IF EXISTS Public cascade;
DROP TABLE IF EXISTS PublicVise cascade;
DROP TABLE IF EXISTS EmissionAimee cascade;
DROP TABLE IF EXISTS GrilleSemaine cascade;
DROP TABLE IF EXISTS AudienceSemaine cascade;
DROP TABLE IF EXISTS Directeur cascade;
DROP TABLE IF EXISTS Invite cascade;
DROP TABLE IF EXISTS Animateur cascade;
DROP TABLE IF EXISTS Acteur cascade;
DROP TABLE IF EXISTS Realisateur cascade;
DROP TABLE IF EXISTS Journal cascade;
DROP TABLE IF EXISTS Documentaire cascade;
DROP TABLE IF EXISTS Serie cascade;
DROP TABLE IF EXISTS Film cascade;
DROP TABLE IF EXISTS Participe cascade;
DROP TABLE IF EXISTS SerieJouee cascade;
DROP TABLE IF EXISTS FilmJoue cascade;
DROP TABLE IF EXISTS ActeurAime cascade;
DROP TABLE IF EXISTS AnimateurAime cascade;
DROP TYPE IF EXISTS JOUR;
DROP TYPE IF EXISTS THEME;
DROP TYPE IF EXISTS TYPESERIE;
DROP VIEW IF EXISTS vJournal;
DROP VIEW IF EXISTS vFilm;
DROP VIEW IF EXISTS vSerie;
DROP VIEW IF EXISTS vDocumentaire;
DROP VIEW IF EXISTS vEmission;
DROP VIEW IF EXISTS vIntervenant;

--Societe(#nom: string)

CREATE TABLE Societe (
    nom VARCHAR(40) PRIMARY KEY
);

--Invite(#nom: string, #prenom: string, nationalite: string, telephone: integer [10]) telephone {key}

CREATE TABLE Invite (
    nom VARCHAR(40),
    prenom VARCHAR(40),
    nationalite VARCHAR(40) NOT NULL,
    telephone INTEGER NOT NULL,
    PRIMARY KEY (nom, prenom)
);

--Animateur(#nom: string, #prenom: string, nationalite: string, telephone: integer [10]) telephone {key}


CREATE TABLE Animateur (
    nom VARCHAR(40),
    prenom VARCHAR(40),
    nationalite VARCHAR(40) NOT NULL,
    telephone INTEGER NOT NULL,
    PRIMARY KEY (nom, prenom)
);

--Acteur(#nom: string, #prenom: string, nationalite: string)


CREATE TABLE Acteur (
    nom VARCHAR(40),
    prenom VARCHAR(40),
    nationalite VARCHAR(40) NOT NULL,
    PRIMARY KEY (nom, prenom)
);

--Realisateur(#nom: string, #prenom: string, nationalite: string)

CREATE TABLE Realisateur (
    nom VARCHAR(40),
    prenom VARCHAR(40),
    nationalite VARCHAR(40) NOT NULL,
    PRIMARY KEY (nom, prenom)
);


--Directeur(#nom: string, #prenom: string, nationalite: string)

CREATE TABLE Directeur (
    nom VARCHAR(40),
    prenom VARCHAR(40),
    nationalite VARCHAR(40) NOT NULL,
    PRIMARY KEY (nom, prenom)
);

--Chaine(#nom: string, siegeSocial : string, plageDiffusion_debut: timestamp, plageDiffusion_fin: timestamp, nomSociete->Societe.nom, nomDirecteur->Directeur.nom, prenomDirecteur->Directeur.prenom)

CREATE TABLE Chaine (
    nom VARCHAR(40) PRIMARY KEY,
    nomSociete VARCHAR(40),
    nomDirecteur VARCHAR(40),
    prenomDirecteur VARCHAR(40),
    siegeSocial VARCHAR(40) NOT NULL,
    plageDiffusionDebut TIME,
    plageDiffusionFin TIME CHECK (plageDiffusionFin > plageDiffusionDebut),
    FOREIGN KEY (nomSociete) REFERENCES Societe(nom),
    FOREIGN KEY (nomDirecteur, prenomDirecteur) REFERENCES Directeur(nom, prenom)
);

--MoyenDiffusion(#type: string)

CREATE TABLE MoyenDiffusion (
    type VARCHAR(40) PRIMARY KEY
);

--DiffuseVers(#chaine->Chaine.nom, #type->MoyenDiffusion.type)

CREATE TABLE DiffuseVers (
    chaine VARCHAR(40),
    type VARCHAR(40),
    FOREIGN KEY (chaine) REFERENCES Chaine(nom),
    FOREIGN KEY (type) REFERENCES MoyenDiffusion(type),
    PRIMARY KEY (chaine, type)
);

--Emission(#nom: sting, pays: string, VO: bool)

CREATE TABLE Emission (
    nom VARCHAR(40) PRIMARY KEY,
    pays VARCHAR(40) NOT NULL,
    VO BOOLEAN
);

--Journal(#nom->Emission.nom,numero: integer, nomAnimateur->Animateur.nom, prenomAnimateur->Animateur.prenom) numero {key}
CREATE TABLE Journal (
	nom VARCHAR(40),
    numero INTEGER,
    nomAnimateur VARCHAR(40) NOT NULL,
    prenomAnimateur VARCHAR(40) NOT NULL,
    FOREIGN KEY (nomAnimateur, prenomAnimateur) REFERENCES Animateur(nom, prenom),
    FOREIGN KEY (nom) REFERENCES Emission(nom),
    PRIMARY KEY(nom,numero)
);

--Documentaire(#nom->Emission.nom, studio: string, theme: enum{economie,animal,politique}, nomAnimateur->Animateur.nom, prenomAnimateur->Animateur.prenom, nomRealisateur->Realisateur.nom, prenomRealisateur->Realisateur.prenom)

CREATE TYPE THEME AS ENUM ('Economie', 'Animal', 'Politique', 'Chasse et Peche');

CREATE TABLE Documentaire (
	nom VARCHAR(40) PRIMARY KEY,
    studio VARCHAR(40) NOT NULL,
    theme THEME,
    nomAnimateur VARCHAR(40) NOT NULL,
    prenomAnimateur VARCHAR(40) NOT NULL,
    FOREIGN KEY (nomAnimateur,prenomAnimateur) REFERENCES Animateur(nom, prenom),
    FOREIGN KEY (nom) REFERENCES Emission(nom)
);

--Serie(#nom->Emission.nom, studio: string, type: enum{drame,action,humour})

CREATE TYPE TYPESERIE AS ENUM ('Drame', 'Action', 'Humour', 'Tele Realite');

CREATE TABLE Serie (
	nom VARCHAR(40) PRIMARY KEY,
    studio VARCHAR(40) NOT NULL,
    type TYPESERIE,
    nomRealisateur VARCHAR(40) NOT NULL,
    prenomRealisateur VARCHAR(40) NOT NULL,
    FOREIGN KEY (nomRealisateur, prenomRealisateur) REFERENCES Realisateur(nom, prenom),
    FOREIGN KEY (nom) REFERENCES Emission(nom)
);

--Film(#nom->Emission.nom, studio: string, nomRealisateur->Realisateur.nom, prenomRealisateur->Realisateur.prenom)
CREATE TABLE Film (
	nom VARCHAR(40) PRIMARY KEY,
    studio VARCHAR(40) NOT NULL,
    nomRealisateur VARCHAR(40) NOT NULL,
    prenomRealisateur VARCHAR(40) NOT NULL,
    FOREIGN KEY (nomRealisateur, prenomRealisateur) REFERENCES Realisateur(nom, prenom),
    FOREIGN KEY (nom) REFERENCES Emission(nom)
);

--Public(type: string)
CREATE TABLE Public (
    type VARCHAR(40) PRIMARY KEY
);

--PublicVise(#emission->Emission.nom, #public->Public.type)

CREATE TABLE PublicVise (
	emission VARCHAR(40),
	public VARCHAR(40),
    FOREIGN KEY (emission) REFERENCES Emission(nom),
    FOREIGN KEY (public) REFERENCES Public(type),
    PRIMARY KEY (emission, public)

);

--EmissionAimee(#emission->Emission.nom, #public->Public.type)
CREATE TABLE EmissionAimee (
	emission VARCHAR(40),
	public VARCHAR(40),
    FOREIGN KEY (emission) REFERENCES Emission(nom),
    FOREIGN KEY (public) REFERENCES Public(type),
    PRIMARY KEY (emission,public)
);

--GrilleSemaine(#chaine->Chaine.nom, #emission->Emission.nom, #semaineDiffusion: integer[52], #jourDiffusion: enum{L,M,Me,J,V,S,D}, #heureDiffusionDebut: timestamp, #heureDiffusionFin: timestamp)

CREATE TYPE JOUR AS ENUM ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');

CREATE TABLE GrilleSemaine (
	chaine VARCHAR(40),
	emission VARCHAR(40),
    semaineDiffusion INTEGER NOT NULL,
    jourDiffusion JOUR,
    heureDiffusionDebut TIME NOT NULL,
    heureDiffusionFin TIME CHECK(heureDiffusionDebut < heureDiffusionFin),
    FOREIGN KEY (chaine) REFERENCES Chaine(nom),
    FOREIGN KEY (emission) REFERENCES Emission(nom),
    PRIMARY KEY (chaine,emission,semaineDiffusion, jourDiffusion,heureDiffusionDebut,heureDiffusionFin)
);

--AudienceSemaine(#date: date,#chaine->Chaine.nom, #emission->Emission.nom, semaine->GrilleSemaine.semaineDiffusion, jour->GrilleSemaine.jourDiffusion, heureDebut->GrilleSemaine.heureDiffusion_debut, heureFin->GrilleSemaine.heureDiffusion_fin, audience : integer)

CREATE TABLE AudienceSemaine (
	date DATE,
	audience INTEGER,
	chaine VARCHAR(40),
	emission VARCHAR(40),
	semaine INTEGER,
	jour JOUR,
	heureDebut TIME,
	heureFin TIME,
    FOREIGN KEY (chaine, emission, semaine, jour, heureDebut, heureFin) REFERENCES GrilleSemaine(chaine, emission, semaineDiffusion, jourDiffusion, heureDiffusionDebut, heureDiffusionFin),
    PRIMARY KEY (date,chaine,emission,semaine,jour,heureDebut,heureFin)
);


--Participe(#inviteNom->Invite.nom, #invitePrenom->Invite.prenom, #journal->Emission.nom)
--EmissionAimee(#emission->Emission.nom, #public->Public.type)
CREATE TABLE Participe (
	inviteNom VARCHAR(40),
	invitePrenom VARCHAR(40),
	journal VARCHAR(40),
	numero INTEGER,
    FOREIGN KEY (inviteNom, invitePrenom) REFERENCES Invite(nom, prenom),
    FOREIGN KEY (journal) REFERENCES Emission(nom),
    PRIMARY KEY(inviteNom,invitePrenom,journal,numero)
);

--SerieJouee(#acteurNom->Acteur.nom, #acteurPrenom->Acteur.prenom, #serie->Emission.nom)
CREATE TABLE SerieJouee (
	acteurNom VARCHAR(40) NOT NULL,
	acteurPrenom VARCHAR(40) NOT NULL,
	serie VARCHAR(40) NOT NULL,
    FOREIGN KEY (acteurNom, acteurPrenom) REFERENCES Acteur(nom, prenom),
    FOREIGN KEY (serie) REFERENCES Emission(nom),
    PRIMARY KEY (acteurNom,acteurPrenom,serie)
);

--FilmJoue(#acteurNom->Acteur.nom, #acteurPrenom->Acteur.prenom, #film->Emission.nom)

CREATE TABLE FilmJoue (
	acteurNom VARCHAR(40) NOT NULL,
	acteurPrenom VARCHAR(40) NOT NULL,
	film VARCHAR(40) NOT NULL,
    FOREIGN KEY (acteurNom, acteurPrenom) REFERENCES Acteur(nom, prenom),
    FOREIGN KEY (film) REFERENCES Emission(nom),
    PRIMARY KEY (acteurNom,acteurPrenom,film)
);

--ActeurAime(#acteurNom->Acteur.nom, #acteurPrenom->Acteur.prenom, #public->Public.type)

CREATE TABLE ActeurAime (
	acteurNom VARCHAR(40) NOT NULL,
	acteurPrenom VARCHAR(40) NOT NULL,
	public VARCHAR(40) NOT NULL,
    FOREIGN KEY (acteurNom, acteurPrenom) REFERENCES Acteur(nom, prenom),
    FOREIGN KEY (public) REFERENCES Public(type),
    PRIMARY KEY (acteurNom,acteurPrenom,public)
);

--AnimateurAime(#animateurNom->Animateur.nom, #animateurPrenom->Animateur.prenom, #public->Public.type)

CREATE TABLE AnimateurAime (
	animateurNom VARCHAR(40) NOT NULL,
	animateurPrenom VARCHAR(40) NOT NULL,
	public VARCHAR(40) NOT NULL,
    FOREIGN KEY (animateurNom, animateurPrenom) REFERENCES Animateur(nom, prenom),
    FOREIGN KEY (public) REFERENCES Public(type),
    PRIMARY KEY (animateurNom, animateurPrenom, public)
);

CREATE VIEW vJournal AS
SELECT Emission.nom as "Journal", numero as "Numero", pays as "Pays", nomAnimateur as "Nom de l'animateur",prenomAnimateur as "Prenom de l'animateur", vo as "Version originale"
FROM Journal, Emission
WHERE Journal.nom = Emission.nom;

CREATE VIEW vFilm AS
SELECT Emission.nom as "Film", Film.studio as "Studio de production", Emission.pays as "Pays", Film.nomRealisateur as "Nom du realisateur",Film.prenomRealisateur as "Prenom du realisateur", Emission.vo as "Version originale"
FROM Film, Emission
WHERE Film.nom = Emission.nom;

CREATE VIEW vSerie AS
SELECT Emission.nom as "Serie", Serie.type as "Type de serie", Serie.studio as "Studio de production", Emission.pays as "Pays", Serie.nomRealisateur as "Nom du realisateur",Serie.prenomRealisateur as "Prenom du realisateur", Emission.vo as "Version originale"
FROM Serie, Emission
WHERE Serie.nom = Emission.nom;

CREATE VIEW vDocumentaire AS
SELECT Emission.nom as "Documentaire", Documentaire.theme as "Theme du documentaire", studio as "Studio de production", Emission.pays as "Pays", Documentaire.nomAnimateur as "Nom de l'animateur", Documentaire.prenomAnimateur as "Prenom de l'animateur", Emission.vo as "Version originale"
FROM Documentaire, Emission
WHERE Documentaire.nom = Emission.nom;

CREATE VIEW vEmission AS
SELECT Journal.nom FROM Journal
UNION SELECT Film.nom FROM Film
UNION SELECT Serie.nom FROM Serie
UNION SELECT Documentaire.nom FROM Documentaire;


CREATE VIEW vIntervenant AS
SELECT nom, prenom,nationalite FROM Directeur
UNION SELECT nom, prenom, nationalite FROM Acteur
UNION SELECT nom, prenom, nationalite FROM Realisateur
UNION SELECT nom, prenom, nationalite FROM Animateur
UNION SELECT nom, prenom, nationalite FROM Invite;