# MLD - Gestion des chaînes de télévision

## Relations

### Justification des héritages

- On choisit un héritage absorbé par classes filles au niveau de la classe Intervenant. En effet, l'héritage est exclusif, il n'y a aucune association N:M sur la classe mère et les classes filles sont impliquées dans beaucoup d'associations propres.

- On choisit un héritage par référence (des filles vers la mère) au niveau de la classe Emission. En effet, bien que la classe mère soit abstraite et l'héritage exclusif, on ne choisit pas l'héritage classe fille car la classe mère est liée par trois relations N:M propres. On ne choisit pas non plus une absorption par classe mère car l'héritage est non-complet (nombreux attributs et relations propres aux classes filles).

### Tables

```
Société(#nom: string)
```

```
Chaine(#nom: string, siègeSocial : string, plageDiffusion_debut: time, plageDiffusion_fin: time, nomSociété->Société.nom, nomDirecteur->Directeur.nom, prénomDirecteur->Directeur.prénom)
```

```
MoyenDiffusion(#type: string)
```

```
DiffuseVers(#chaine->Chaine.nom, #type->MoyenDiffusion.type)
```

```
Emission(#nom: sting, pays: string, VO: bool)
```

```
Public(type: string)
```

```
PublicVisé(#emission->Emission.nom, #public->Public.type)
```

```
EmissionAimée(#emission->Emission.nom, #public->Public.type)
```

```
GrilleSemaine(#chaine->Chaine.nom,#émission->Emission.nom, #semaineDiffusion: date, #jourDiffusion: enum{L,M,Me,J,V,S,D}, #heureDiffusion_debut: time, #heureDiffusion_fin: time)
```

```
AudienceSemaine(#date: date,#chaine->Chaine.nom, #émission->Emission.nom, semaine->GrilleSemaine.semaineDiffusion, jour->GrilleSemaine.jourDiffusion, heureDebut->GrilleSemaine.heureDiffusion_debut, heureFin->GrilleSemaine.heureDiffusion_fin, audience : integer)
```

```
Directeur(#nom: string, #prénom: string, nationalité: string)
```

```
Invité(#nom: string, #prénom: string, nationalité: string, téléphone: integer [10]) téléphone {key}
```

```
Animateur(#nom: string, #prénom: string, nationalité: string, téléphone: integer [10]) téléphone {key}
```

```
Acteur(#nom: string, #prénom: string, nationalité: string)
```

```
Réalisateur(#nom: string, #prénom: string, nationalité: string)
```

```
Journal(#nom->Emission.nom,numéro: integer, nomAnimateur->Animateur.nom, prénomAnimateur->Animateur.prénom) numéro {key}
```

```
Documentaire(#nom->Emission.nom, studio: string, thème: enum{économie,animal,politique}, nomAnimateur->Animateur.nom, prénomAnimateur->Animateur.prénom, nomRéalisateur->Réalisateur.nom, prénomRéalisateur->Réalisateur.prénom)
```

```
Série(#nom->Emission.nom, studio: string, type: enum{drame,action,humour})
```

```
Film(#nom->Emission.nom, studio: string, nomRéalisateur->Réalisateur.nom, prénomRéalisateur->Réalisateur.prénom)
```

```
Participe(#invitéNom->Invité.nom, #invitéPrénom->Invité.prénom, #journal->Emission.nom)
```

```
SérieJouée(#acteurNom->Acteur.nom, #acteurPrénom->Acteur.prénom, #série->Emission.nom)
```

```
FilmJoué(#acteurNom->Acteur.nom, #acteurPrénom->Acteur.prénom, #film->Emission.nom)
```

```
ActeurAimé(#acteurNom->Acteur.nom, #acteurPrénom->Acteur.prénom, #public->Public.type)
```

```
AnimateurAimé(#animateurNom->Animateur.nom, #animateurPrénom->Animateur.prénom, #public->Public.type)
```


### Contraintes

- Tous les attributs sont ```NOT NULL```

```plageDiffusionDébut<plageDiffusionFin
IF Emission.pays='France' Emission.VO IS NULL
```

- Contraintes de l'héritage par référence avec classe mère abstraite:
```
Projection(Série,nom) ∩ Projection(Film,nom) ∩ Projection(Documentaire,nom) ∩ Projection(Journal,nom)= {}
```

```
Projection(Emission, nom) = Projection(Journal, nom) ∪ Projection(Documentaire, nom) ∪ Projection(Série, nom) ∪ Projection(Film, nom)
```

- Contraintes de l'héritage par classe filles avec classe mère non-astraite:
```
Projection(Directeur, nom, prénom) ∩ Projection(Invité, nom, prénom) ∩ Projection(Animateur, nom, prénom) ∩ Projection(Acteur, nom, prénom) ∩ Projection(Réalisateur, nom, prénom)={}
```

```
Projection(Intervenant, nom, prénom) ∩
 (Projection(Directeur, nom, prénom) ∪ Projection (Invité, nom, prénom) ∪ Projection(Animateur, nom, prénom) ∪ Projection(Acteur, nom, prénom) ∪ Projection(Réalisateur, nom, prénom))={}
```

- Contraintes de cardinalité minimale:

```
Projection(MoyenDiffusion,type) = Projection(DiffuseVers,type)
```

```
Projection(Directeur,nom,prénom) = Projection(Chaine,nomDirecteur,prénomDirecteur)
```

```
Projection(GrilleSemaine,nom) = Projection(AudienceSemaine,emission)
```

```
Projection(Public,type) = Projection(PublicVisé,public)
```

```
Documentaire.nomAnimateur Documentaire.prénomAnimateur NULL ABLE
```

### Vues
```
vSérie=Projection(Jointure(Emission,Série,Emission.nom=Série.nom),nom, pays, VO, studio, type)
```
```
vFilm=Projection(Jointure(Emission,Film,Emission.nom=Film.nom), nom, pays, VO, studio)
```
```
vDocumentaire=Projection(Jointure(Emission,Documentaire,Emission.nom=Documentaire.nom), nom, pays, VO, studio, thème)
```
```
vJournal=Projection(Jointure(Emission,Journal,Emission.nom=Journal.nom), nom, pays, VO, numéro)
```
```
vIntervenants=Projection(Directeur,nom,prénom,nationalité) ∪ Projection(Invité,nom,prénom,nationalité) ∪ Projection(Animateur,nom,prénom,nationalité) ∪ Projection(Acteur,nom,prénom,nationalité) ∪ Projection(Réalisateur,nom,prénom,nationalité)
```

## DF et Normalisation

### Etude de quelques classes type

**Chaine**

```
nom -> siègeSocial, plageDiffusion_debut, plageDiffusion_fin, nomSociété, nomDirecteur, prénomDirecteur
```

- Possède une clé et attributs atomiques ->1NF 

- Clé composée d'un seul attribut -> 2NF 

- Aucun des autres attributs n'est une clé candidate mais ils dépendent tous directement du nom de la chaine -> 3NF

**AudienceSemaine**

```
date, émission -> audience
```

- Possède une clé composée et des attributs atomiques -> 1NF

- Pas de sous-partie de la clé qui détermine un attribut non-clé -> 2NF

- Aucun attribut non clé ne dépend d’un autre attribut non clé -> 3NF

**ActeurAimé**
```
acteurNom -> {}
acteurPrénom -> {}
public -> {}
```
- Relation toute clé -> 3NF


### Conclusion générale

- Tous les attributs sont atomiques: 1NF

- Tous les attributs n'appartenant à aucune clé candidate ne dépendent pas d'une partie seulement d'une clé candidate: 2NF
- 
- Tous les attributs n'appartenant à aucune clé candidate ne dépendent directement que de clés candidates: 3NF

**Notre modélisation est normalisée**


